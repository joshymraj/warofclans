﻿namespace WarOfClans
{
    public enum SoldierState
    {
        Idle,
        Encountering,
        Attacking,
        Retreating,
        Dead
    }

    public enum GameState
    {
        Loading = 0,
        Playing,
        Paused,
        Ended
    }

    public enum Placeable
    {
        Infantry,
        Obstacle
    }

    public enum Damageable
    {
        Infantry,
        Building
    }

    public enum BuildingType
    {
        HQ,
        Tent
    }

    public enum Clan
    {
        Red,
        Blue
    }

    public enum CameraView
    {
        TopView,
        IsometricView
    }
}
