using UnityEngine;

namespace WarOfClans
{
    public interface IInfantry : IDamageableGameObject
    {
        int ID { get; set; }

        Clan Clan { get; set; }        

        SoldierState GetState();

        void SetState(SoldierState soldierState);

        IDamageableGameObject GetEnemy();

        void Init(InfantryData infantryData);

        void Encounter(IDamageableGameObject enemy);

        void Attack();        
    }
}
