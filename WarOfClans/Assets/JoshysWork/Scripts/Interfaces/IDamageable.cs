using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WarOfClans
{
    public interface IDamageableGameObject
    {
        Damageable DamageableType { get; set; }       

        IHealthUI HealthUI { get; set; }

        bool IsActive();

        GameObject GetGameObject();

        Transform GetHitPoint();

        void TakeDamage(float damage);
    }
}
