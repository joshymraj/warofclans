using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace WarOfClans
{
    public class Infantry : MonoBehaviour, IAIInfantry
    {
        [SerializeField] private SoldierEventChannel soldierEventChannel;

        [Header("Animation States")]
        [SerializeField] private List<string> attackAnimationStates;
        [SerializeField] private float attackAnimationDuration;
        [Space()]
        [SerializeField] private List<string> deathAnimationStates;
        [SerializeField] private float deathAnimationDuration;
        [Space()]
        [SerializeField] private string damageAnimationState;
        [Space()]
        [SerializeField] private string moveAnimationState;

        private Animator animator;
        private INavigation navigator;
        private InfantryData soldierData;
        private float armourHealth;
        private float health;
        private SoldierState state;
        private IInfantry enemyInfantry;
        private IDamageableGameObject enemyDamageable;
        private Transform enemyHitPoint;

        public int ID
        {
            get;
            set;
        }

        public Clan Clan
        {
            get;
            set;
        }

        public IHealthUI HealthUI
        {
            get;
            set;
        }

        public Damageable DamageableType
        {
            get;
            set;
        }        

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (state == SoldierState.Encountering)
            {
                navigator.UpdateDestination(enemyHitPoint.position);
            }

            if (state != SoldierState.Dead)
            {
                HealthUI?.SetPosition(transform.position);
            }
        }

        private void RegisterEvents()
        {
            if (soldierEventChannel != null)
            {
                navigator.OnDestinationReached += OnReachedTarget;
            }
        }

        private IEnumerator AttackTillDeath()
        {
            while (state == SoldierState.Attacking && enemyDamageable.IsActive())
            {
                animator.SetTrigger(attackAnimationStates[Random.Range(0, attackAnimationStates.Count)]);
                enemyDamageable.TakeDamage(soldierData.damagePerAttack);
                yield return new WaitForSeconds(Random.Range(soldierData.maxAttackSpeed, soldierData.minAttackSpeed)); //TODO: Change the name to attackReflex which makes more sense.
            }

            if (state == SoldierState.Attacking)
            {
                state = SoldierState.Idle;
                OnStateChanged();
            }            
        }

        private IEnumerator Die()
        {
            animator.SetBool(deathAnimationStates[Random.Range(0, deathAnimationStates.Count)], true);

            yield return new WaitForSeconds(deathAnimationDuration);

            OnStateChanged();
        }

        public bool IsActive()
        {
            return health > 0;
        }

        public IDamageableGameObject GetEnemy()
        {
            return enemyDamageable;
        }

        public SoldierState GetState()
        {
            return state;
        }

        public void SetState(SoldierState soldierState)
        {
            switch(soldierState)
            {
                case SoldierState.Idle:
                    {
                        animator.SetFloat(moveAnimationState, 0);
                        state = SoldierState.Idle;
                        navigator.StopNavigation();
                    }
                    break;

                default:
                    break;
            }

            if(GameManager.Instance.GameState == GameState.Playing)
            {
                OnStateChanged();
            }
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public Transform GetHitPoint()
        {
            return gameObject.transform;
        }

        public void Init(InfantryData infantryData)
        {
            soldierData = infantryData;
            state = SoldierState.Idle;
            armourHealth = infantryData.armourStrength;
            health = 100;
            if (soldierData.maxAttackSpeed < attackAnimationDuration)
            {
                soldierData.maxAttackSpeed = attackAnimationDuration;
            }
            RegisterEvents();
        }

        public void Init(InfantryData infantryData, INavigation navigationController)
        {
            soldierData = infantryData;
            navigator = navigationController;
            state = SoldierState.Idle;
            armourHealth = infantryData.armourStrength;
            health = 100;
            if (soldierData.maxAttackSpeed < attackAnimationDuration)
            {
                soldierData.maxAttackSpeed = attackAnimationDuration;
            }
            RegisterEvents();
        }

        public void Attack()
        {
            if (state == SoldierState.Dead)
            {
                return;
            }

            state = SoldierState.Attacking;

            Debug.Log(string.Format("{0} {1} is attacking {2}", gameObject.name, ID, enemyDamageable.GetGameObject().name));

            OnStateChanged();
            StartCoroutine(AttackTillDeath());
        }

        public void Encounter(IDamageableGameObject enemyDamageableGameObject)
        {
            if (state == SoldierState.Dead)
            {
                return;
            }

            enemyInfantry = null;

            if(enemyDamageableGameObject.DamageableType == Damageable.Infantry)
            {
                enemyInfantry = enemyDamageableGameObject.GetGameObject().GetComponent<Infantry>();
            }

            enemyDamageable = enemyDamageableGameObject;
            enemyHitPoint = enemyDamageableGameObject.GetHitPoint();
            navigator.StartNavigation(enemyHitPoint.position, soldierData.maxMovementSpeed);
            animator.SetFloat(moveAnimationState, soldierData.maxMovementSpeed);
            state = SoldierState.Encountering;

            Debug.Log(string.Format("{0} is encountering {1}", gameObject.name, enemyDamageable.GetGameObject().name));

            OnStateChanged();
        }

        public void TakeDamage(float damage)
        {
            if (state == SoldierState.Dead)
            {
                return;
            }
           
            animator.SetTrigger(damageAnimationState);

            if(armourHealth > 0)
            {
                armourHealth -= damage;
                return;
            }

            health -= damage;
            HealthUI?.ShowHealth(health);

            if (health <= 0)
            {
                state = SoldierState.Dead;

                if (HealthUI != null)
                {
                    Destroy(HealthUI.GetGameObject());
                }

                StartCoroutine(Die());
            }
        }

        private void OnStateChanged()
        {
            soldierEventChannel.NotifyPlayerStateChanged(this, state);
        }

        private void OnReachedTarget()
        {
            animator.SetFloat(moveAnimationState, 0);
            state = SoldierState.Idle;
            soldierEventChannel.NotifyPlayerReachedTarget(this);
        }        
    }
}
