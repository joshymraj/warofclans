using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using DG.Tweening;

namespace WarOfClans
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Image loadingFader;
        [SerializeField] private BattleManager battleManager;

        [Header("Event Channels")]
        [SerializeField] private UIEventChannel uiEventChannel;

        public static GameManager Instance
        {
            get;
            private set;
        }

        public GameState GameState
        {
            get;
            set;
        }

        private void Awake()
        {
            DontDestroyOnLoad(this);
            Instance = this;
            Instance.GameState = GameState.Loading;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private void Start()
        {
            SubscribeEvents();
            StartGame();
        }

        private void SubscribeEvents()
        {
            if (uiEventChannel != null)
            {
                uiEventChannel.OnGamePaused += PauseGame;
                uiEventChannel.OnGameResumed += ResumeGame;
                uiEventChannel.OnGameRestart += RestartGame;
            }
        }

        private void StartGame()
        {
            loadingFader.DOFade(0, 1).OnComplete(() =>
            {              
                battleManager.StartBattle();
            });
        }

        private void PauseGame()
        {
            //TODO
        }

        private void ResumeGame()
        {
            //TODO
        }

        private void RestartGame()
        {
            SceneManager.LoadScene("LoadingScene");
        }
    }
}
