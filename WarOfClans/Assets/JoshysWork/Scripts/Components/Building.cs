using UnityEngine;


namespace WarOfClans
{
    public class Building : MonoBehaviour, IDamageableGameObject
    {       
        public Clan clan;

        public BuildingType buildingType;

        [SerializeField] private BuildingEventChannel buildingEventChannel;

        [SerializeField] private Damageable damageableType;        

        [SerializeField] private float strength = 200;

        [SerializeField] private Transform[] hitPoints;

        public Damageable DamageableType
        {
            get => damageableType;
            set => damageableType = value;
        }

        public IHealthUI HealthUI
        {
            get;
            set;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public Transform GetHitPoint()
        {
            return hitPoints[Random.Range(0, hitPoints.Length)];
        }

        public bool IsActive()
        {
            return strength > 0;
        }

        public void TakeDamage(float damage)
        {
            if (strength < 0)
            {
                return;
            }

            strength -= damage;

            HealthUI?.ShowHealth(strength);

            if (strength <= 0)
            {
                buildingEventChannel.NotifyCollapsed(this);
            }
        }
    }
}
