﻿using UnityEngine;
using UnityEngine.Playables;

namespace WarOfClans
{
	public class CinematicsManager : MonoBehaviour
	{
		[SerializeField] private PlayableDirector redTowerDestructionCinematics;
		[SerializeField] private PlayableDirector blueTowerDestructionCinematics;

		[SerializeField] private UIEventChannel uiEventChannel;
		[SerializeField] private BuildingEventChannel buildingEventChannel;

		private Animator animator;
		private CameraView cameraView;

        private void Awake()
        {
			animator = GetComponent<Animator>();
			cameraView = CameraView.IsometricView;

			SubscribeEvents();
        }

		private void SubscribeEvents()
        {
			if(uiEventChannel != null)
            {
				uiEventChannel.OnCameraSwitchTapped += SwitchCamera;
            }

			if(buildingEventChannel != null)
            {
				buildingEventChannel.OnCollapased += PlayBuildingCollapse; 
            }
		}

		public void SwitchCamera()
        {
			cameraView = cameraView == CameraView.IsometricView ? CameraView.TopView : CameraView.IsometricView;
			animator.SetBool("Isometric", cameraView == CameraView.IsometricView);
        }

        public void PlayBuildingCollapse(Building building)
		{
			if(building.buildingType != BuildingType.HQ)
            {
				return;
            }

			switch(building.clan)
            {
				case Clan.Red:
					redTowerDestructionCinematics.Play();
					break;

				case Clan.Blue:
					blueTowerDestructionCinematics.Play();
					break;

				default:
					break;
            }
		}


	}
}