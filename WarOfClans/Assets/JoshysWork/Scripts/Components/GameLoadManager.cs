using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using DG.Tweening;

namespace UnityRoyale
{
    public class GameLoadManager : MonoBehaviour
    {
        [SerializeField] private Image loadingFader;
        [SerializeField] private GameObject playButton;

        void Start()
        {
            LoadGame();
        }

        private void LoadGame()
        {
            loadingFader.DOFade(0, 1).OnComplete(() => playButton.SetActive(true));
        }

        public void PlayGame()
        {
            playButton.SetActive(false);
            loadingFader.DOFade(1, 1).SetDelay(1).OnComplete(() =>
            {
                SceneManager.LoadScene("GameScene");
            });
        }
    }
}
