using System;

using UnityEngine;

namespace WarOfClans
{
    [CreateAssetMenu(fileName = "BuildingEventChannel", menuName = "War Of Clans/Building Event Channel")]
    public class BuildingEventChannel : ScriptableObject
    {
        public Action<Building> OnConstructionStarted
        {
            get;
            set;
        }

        public Action<Building> OnConstructionCompleted
        {
            get;
            set;
        }

        public Action<Building> OnCollapased
        {
            get;
            set;
        }

        public void NotifyConstructionStarted(Building building)
        {
            OnConstructionStarted?.Invoke(building);
        }

        public void NotifyConstructionCompleted(Building building)
        {
            OnConstructionCompleted?.Invoke(building);
        }

        public void NotifyCollapsed(Building building)
        {
            OnCollapased?.Invoke(building);
        }
    }
}
